#include <iostream>
#include "median.hpp"


inline int mediana(int* A, int* B, int N, int M) {
    return median(A, B, N, M);
}


int main() {
    int N = 6;
    int A[] = {3, 5, 9, 10, 11, 15};

    int M = 5;
    int B[] = {4, 7, 8, 12, 13};

    std::cout << "La mediana es " << mediana(A, B, N, M) << "\n";

    return 0;
}
