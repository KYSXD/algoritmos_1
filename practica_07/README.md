# Practica 7

## Descripción

### Objetivos

- Familizarizar al estudiantes con las diferentes búsquedas que existen.
- Que el estudiante sea capaz de realizar una búsqueda binaria.

### Problema 1

Se te dará un arreglo de números enteros ordenados.
La tarea es poder responder a preguntas de la forma
"¿Cuantas veces aparece el número _x_ en el arreglo?".

#### Ejercicio

En este caso no será necesario implementar una clase ni una librería.
Simplemente escribe la función que resuelve el problema.
Esta función debe tener complejidad _O(log n)_.

#### Compilación

```bash
$ g++ --std=c++11 Apariciones.cpp -o Apariciones
```

*Nota del desarrollador: Se sugiere usar la siguiente instrucción:*

```bash
$ g++ --std=c++11 Apariciones.cpp -o Apariciones.o
```

### Problema 2

Se te darán dos arreglos ordenados,
el primer arreglo con tamaño par y el segundo con tamaño impar.
Deberás devolver la mediana del
arreglo resultante de unir los dos arreglos iniciales.

#### Ejercicio

En este caso no será necesario implementar una clase ni una librería.
Simplemente escribe la función que resuelve el problema.
La función debe resolver el problema en
_O(log N + log M)_
(Nota que al usar O indicia que tu programa puede tener una menor complejidad)

#### Compilación

```bash
$ g++ --std=c++11 Mediana.cpp -o Mediana
```

*Nota del desarrollador: Se sugiere usar la siguiente instrucción:*

```bash
$ g++ --std=c++11 Mediana.cpp -o Mediana.o
```

## Reporte

### Marco teórico

Una busqueda lineal
es un método de busqueda que recorre secuencialmente
todos los elementos de una lista.
En el peor de los casos toma complejidad `O(n)`.

Una busqueda binaria (o busqueda logaritmica)
es un método de busqueda que aprovecha el orden del arreglo de entrada
para buscar comparando el elemento medio del arreglo,
devolviendo el elemento si este satisface la busqueda
o buscando en los intervalos mayor o menor dependiendo de la comparacion
con el elemento a buscar.

### Descripción del programa

`ocurrences.hpp`
implementa dos funciones recursivas que buscan (de forma binaria)
la primera y última ocurrencia del elemento a buscar
y una que llama a estas y devuelve la diferencia entre los indices obtenidos.

`median.hpp`
implementa una función que busca la mediana del que sería
el arreglo resultante de combinar dos arreglos ordenados.
Este realiza una busqueda binaria en el arreglo de menor tamaño
para ajustar el centro en ambos arreglos y devolver esta coincidencia.

### Resultados obtenidos

Se cuentan las coincidencias de un entero en una lista como se espera.

Se encuentra la mediana del que sería el arreglo resultante
de unir dos arreglos en uno como se espera.

### Problemas encontrados

Ninguno.

### Referencias

- La clase donde se mencionó el tema.
- https://en.cppreference.com
- https://www.geeksforgeeks.org/median-of-two-sorted-arrays-of-different-sizes/
- https://www.geeksforgeeks.org/count-number-of-occurrences-or-frequency-in-a-sorted-array/
