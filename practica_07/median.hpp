#pragma once
#include <algorithm>
#include <limits.h>


int median(int* arr_a, int *arr_b, int arr_a_len, int arr_b_len) {
    // use the shortest array
    if(arr_a_len > arr_b_len) {
        return median(arr_b, arr_a, arr_b_len, arr_a_len);
    }

    int start = 0;
    int end = arr_a_len;
    int real_mid_in_merged_array = (arr_a_len + arr_b_len + 1) / 2;

    // loop while makes sense
    while (start <= end) {
        int mid = (start + end) / 2;
        int left_a_len = mid;
        int left_b_len = real_mid_in_merged_array - mid;

        // handle overflow
        int left_a
            = (left_a_len > 0) ? arr_a[left_a_len - 1] : INT_MIN;
        int left_b
            = (left_b_len > 0) ? arr_b[left_b_len - 1] : INT_MIN;
        int right_a
            = (left_a_len < arr_a_len) ? arr_a[left_a_len] : INT_MAX;
        int right_b
            = (left_b_len < arr_b_len) ? arr_b[left_b_len] : INT_MAX;

        // if correct partition is correct
        if (left_a <= right_b and left_b <= right_a) {
            // handle even case
            if ((arr_b_len + arr_a_len) % 2 == 0)
                return (
                    std::max(left_a, left_b) + std::min(right_a, right_b)
                ) / 2.0;
            // handle odd case
            return std::max(left_a, left_b);
        }
        // dec size of partition
        else if (left_a > right_b) {
            end = mid - 1;
        }
        // inc size of partition
        else
            start = mid + 1;
    }

    // default return
    return 0;
}
