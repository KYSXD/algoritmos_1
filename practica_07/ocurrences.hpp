#pragma once


// recursive binary search for the first ocurrence
int first(int* arr, int low, int high, int match, int arr_len) {
    if(high >= low) {
        int mid = (low + high) / 2;
        if(
            (mid == 0 || match > arr[mid - 1]) &&
            arr[mid] == match
        ) {
            return mid;
        }
        else if(match > arr[mid]) {
            return first(arr, (mid + 1), high, match, arr_len);
        }
        else {
            return first(arr, low, (mid - 1), match, arr_len);
        }
    }

    return -1;
}


// recursive binary search for the last ocurrence
int last(int* arr, int low, int high, int match, int arr_len) {
    if(high >= low) {
        int mid = (low + high) / 2;
        if(
            ( mid == arr_len - 1 || match < arr[mid + 1]) &&
            arr[mid] == match
        ) {
            return mid;
        }
        else if(match < arr[mid]) {
            return last(arr, low, (mid - 1), match, arr_len);
        }
        else {
            return last(arr, (mid + 1), high, match, arr_len);
        }
    }

    return -1;
}


int ocurrences(int* arr, int arr_len, int match) {
    // get first ocurrence
    int i = first(arr, 0, arr_len - 1, match, arr_len);

    // early return if no ocurrence
    if(i == -1)
        return 0;

    // get last ocurrence
    int j = last(arr, i, arr_len - 1, match, arr_len);

    // return difference
    return j - i + 1;
}
