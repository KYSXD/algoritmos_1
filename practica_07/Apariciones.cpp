#include <iostream>
#include "ocurrences.hpp"


inline int apariciones_x(int* arreglo, int N, int x) {
    return ocurrences(arreglo, N, x);
}


int main() {
    int N = 10;
    int arreglo[] = {1, 2, 2, 3, 6, 6, 6, 9, 11, 11};

    std::cout << "El 6 aparece " << apariciones_x(arreglo, N, 6) << " veces\n";
    std::cout << "El 0 aparece " << apariciones_x(arreglo, N, 0) << " veces\n";
    std::cout << "El 4 aparece " << apariciones_x(arreglo, N, 4) << " veces\n";
    std::cout << "El 11 aparece " << apariciones_x(arreglo, N, 11) << " veces\n";

    return 0;
}
