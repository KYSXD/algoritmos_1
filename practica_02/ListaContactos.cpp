#include <iostream>
#include "ListaContactos.hpp"


Contacto::Contacto(string nombre, string numero, string email):
    nombre(nombre),
    numero(numero),
    email(email)
{
    siguiente = NULL;
    anterior = NULL;
}


Contacto::~Contacto() {
}


ListaContactos::ListaContactos() {
    Contacto *pseudo_null = new Contacto("", "", "");
    head = pseudo_null;
    contacto_act = head;
}


ListaContactos::~ListaContactos() {
    Node* tmp = head;
    while(tmp != NULL) {
        Node *to_delete = tmp;
        tmp = tmp->next;

        delete(to_delete);
    }
}


void ListaContactos::agregar_contacto(int n, Contacto* contacto) {
    // TODO: DRY, maybe add move_to(int) at class
    Contacto* current = contacto_act;
    if(n < 0) {
        while(n && current->anterior) {
            n++;
            current = current->anterior;
        }
    }
    else {
        while(n && current->siguiente) {
            n--;
            current = current->siguiente;
        }
    }

    // if n != 0, position wasn't reached
    if(n) {
        std::cout << "[ERROR] Posicion no valida" << std::endl;
    } else {
        // set references in inserted item
        contacto->siguiente = current->siguiente;
        contacto->anterior = current;
        // set references in neighbor items
        if(current->siguiente) {
            current->siguiente->anterior = contacto;
        }
        current->siguiente = contacto;
        // set references in contacto_act
        contacto_act = contacto;
    }
}


void ListaContactos::eliminar_contacto(int n) {
    // TODO: DRY, maybe add move_to(int) at class
    Contacto* current = contacto_act;
    if(n < 0) {
        while(n && current->anterior) {
            n++;
            current = current->anterior;
        }
    }
    else {
        while(n && current->siguiente) {
            n--;
            current = current->siguiente;
        }
    }

    // if n != 0, position wasn't reached
    // if current == head, position is invalid
    if(n || current == head) {
        std::cout << "[ERROR] Posicion no valida" << std::endl;
    } else {
        // set references in neighbor items
        if(current->siguiente) {
            current->siguiente->anterior = current->anterior;
        }
        if(current->anterior) {
            current->anterior->siguiente = current->siguiente;
        }
        // set references in contacto_act
        contacto_act = current->anterior;

        std::cout << "Se ha eliminado el contacto:" << std::endl;
        std::cout << "    Nombre:" << current->nombre << std::endl;
        std::cout << "    Numero:" << current->numero << std::endl;
        std::cout << "    Email:" << current->email << std::endl;
        free(current);
    }
}


void ListaContactos::mostrar_lista() {
    Contacto* current = head;
    int counter = 0;

    std::cout << "#### Lista de contactos ####" << std::endl;

    while(current->siguiente) {
        counter++;
        current = current->siguiente;
        std::cout << counter << ". " << current->nombre << std::endl;
    }

    std::cout << "############################" << std::endl;
}
