# Practica 2

## Descripción

### Objetivos

- Familiarizar al estudiante con la implementación de una lista ligada simple usando clases
- Que el estudiante sea capaz de implementar una lista doblemente ligada

### Ejercicio

Las operaciones que debe ser capaz de realizar son:

- `agregar_contacto(int n, Contacto* contacto)`
  Deberá moverse `n` contactos hacia adelante y agregar un nuevo contacto inmediatamente después.
  El último contacto que afectó queda como el nuevo que acaba de agregar.
- `eliminar_contacto(int n)`
  Deberá moverse `n` contactos hacia adelante y eliminar ese contacto.
  El último contacto que afectó queda determinado como el anterior al que eliminaste.
  No se puede eliminar el contacto `0`.
  Cuando se elimine un contacto deberá aparecer el mensaje:
  ```
  Se ha eliminado el contacto:
    Nombre: nombre_del_contacto
    Numero: 123456789
    Email: jose.vales@cimat.mx
  ```
- `mostrar_lista()`
  Imprime el nombre de todos los contactos en el nombre que están.
  Si en algún momento se intenta acceder a una posición que no es válida se deberá imprimir
  el mensaje `[ERROR] Posicion no valida` y la instruccion no se ejecuta.

En caso de que `n` sea un número negativo en lugar de moverse `n` lugares hacia adelante,
se moverá `n` lugares hacia atrás.

### Compilación

```bash
$ g++ --std=c++11 main.cpp ListaContactos.cpp -o ListaContactos
```

*Nota del desarrollador: Se recomienda usar la siguiente instrucción
(que agrega extensión `.o` al archivo generado):*

```bash
$ g++ --std=c++11 main.cpp ListaContactos.cpp -o ListaContactos.o
```

## Reporte

### Marco teórico

Una lista doblemente ligada es una estructura de datos
compuesta por un conjunto de nodos secuencialmente enlazados.
Cada nodo de la lista consta de tres partes:

- Una referencia al nodo siguiente.
- Una referencia al nodo anterior.
- La información corresponiente a ese nodo.

### Descripción del programa

La librería consta de dos clases,
`Contacto` y `ListaContactos`, que corresponden a
al nodo de la lista ligada y
la encapsulación de la lógica (métodos) para
manipular la lista, respectivamente.

Además,
el método principal `main()`
crea una lista ligada (via la clase `ListaContactos`),
y agrega y elimina contactos.

### Resultados obtenidos

Se crea y manipula satisfactoriamente una lista ligada tal como se requiere.

### Problemas encontrados

Aunque no fue requerido en el problema descrito,
se consideró en la implentación el caso en que
la función `agregar_contacto()` recibe un índice inalcanzable.

### Referencias

- La clase donde se mencionó el tema.
- https://en.cppreference.com
