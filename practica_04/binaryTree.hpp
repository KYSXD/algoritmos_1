#pragma once
#include <cstddef>


/**
 * Node for the binary tree implementation
 */
class Node {
    public:
        int id;
        Node *left, *right;

        Node(int id) : id(id) {
            left = NULL;
            right = NULL;
        }
};


/**
 * Implementation of a binary tree
 */
class BinaryTree {
    public:
        /**
         * Constructs binary tree given inOrder and postOrder traversals
         *
         * @param inOrdr InOrder traversal
         * @param postOrdr PostOrder traversal
         * @param n Number of items within the tree
         */
        BinaryTree(int* inOrdr, int* postOrdr, int n);

        /**
         * Destructucs the binary tree
         */
        ~BinaryTree();

        /**
         * Iterative function to do preOrder traversal of the tree
         *
         * @return array with preOrder traversal of the tree
         */
        int* preOrderTraversal();

        /**
         * Alias for:
         * int* preOrderTraversal()
         *
         * @return array with preOrder traversal of the tree
         */
        inline int* recorrido_preorder() { return preOrderTraversal(); }

    private:
        Node* root;
        int size;

        /**
         * Calls buildAux() with initial values
         *
         * @param inOrdr InOrder traversal
         * @param postOrdr PostOrder traversal
         * @param n Number of items within the tree
         * @return root node of the tree
         */
        Node* buildTree(int inOrdr[], int postOrdr[], int n);

        /**
         * Actual tree constructor
         * Recursive function to build given inOrder and postOrder traversals
         * Assumes inOrder and postOrder are valid traversals for the tree
         *
         * @param inOrdr InOrder traversal
         * @param postOrdr PostOrder traversal
         * @param startIndex Start of the InOrder traversal
         * @param endIndex End of the InOrder traversal
         * @param postOrderIndex Current index of the postOrder traversal
         * @return built binary tree given the above conditions
         */
        Node* buildAux(
            int inOrdr[],
            int postOrdr[],
            int startIndex,
            int endIndex,
            int* postOrderIndex
        );

        /**
         * Function to find index of value in arr[start...end]
         *
         * @param arr InOrder traversal
         * @param startIndex Start of the interval
         * @param endIndex End of the interval
         * @return index of 'value' in the inOrder traversal
         */
        int searchInOrder(
            int arr[],
            int startIndex,
            int endIndex,
            int value
        );

        /**
         * Recursive function to deallocate a node and all of its children
         * Deletes using postOrder traversal
         *
         * @param node Node to delete
         */
        void deleteNode(Node* node);
};
