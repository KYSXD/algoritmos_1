#include <iostream>
#include "ArbolBinario.hpp"

int main() {
    int N = 5;
    int recorridoInorder[] = {3, 1, 7, 2, 8};
    int recorridoPosorder[] = {3, 7, 8, 2, 1};

    ArbolBinario arbol(recorridoInorder, recorridoPosorder, N);
    int* recorridoPreorder = arbol.recorrido_preorder();

    std::cout << "Recorrido en preorder del arbol:\n";
    for (int i = 0; i < N; i++)
        std::cout << recorridoPreorder[i] << " ";

    delete[] recorridoPreorder;

    return 0;
}
