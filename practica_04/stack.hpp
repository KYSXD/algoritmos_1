#pragma once
#include <cstdlib>


/**
 * Node class for stack
 */
template <typename T>
class CNode {
    public:
        T data;
        CNode *next;

        CNode(T& t) : data(t), next(NULL) { }
        ~CNode() { }
};


/**
 * Stack class
 * All methods are based on the ones already explained in previous works
 * @param T The type to be stored
 */
template <typename T>
class CStack {
    private:
        CNode<T>* head;

    public:
        CStack();
        ~CStack();

        void push(T& t);
        void pop();
        T& top();
        bool empty();
};


template <typename T>
CStack<T>::CStack() {
  head = NULL;
}


template <typename T>
CStack<T>::~CStack() {
    CNode<T>* tmp = head;
    while(tmp != NULL) {
        CNode<T>* to_delete = tmp;
        tmp = tmp->next;

        delete to_delete;
    }
}


template <typename T>
void CStack<T>::push(T& t) {
    CNode<T>* insertion = new CNode<T>(t);

    if (head == NULL){
        head = insertion;
    } else {
        insertion->next = head;
        head = insertion;
    }
}


template <typename T>
void CStack<T>::pop() {
    if (head == NULL)
        return;

    CNode<T>* tmp = head;
    head = head->next;

    delete(tmp);
}


template <typename T>
T& CStack<T>::top() {
    return head->data;
}


template <typename T>
bool CStack<T>::empty() {
    return head == NULL;
}
