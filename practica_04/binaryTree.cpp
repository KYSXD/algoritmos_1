#include <cstddef>
#include "binaryTree.hpp"
#include "stack.hpp"


BinaryTree::BinaryTree(int* inOrdr, int* postOrdr, int n) : size(n) {
    root = buildTree(inOrdr, postOrdr, n);
}


BinaryTree::~BinaryTree() {
    deleteNode(root);
}


int* BinaryTree::preOrderTraversal() {
    int* idStorage = new int[size];
    int currentIndex = 0;

    CStack<Node*> nodeStack;
    nodeStack.push(root);

    /**
     * While stack has nodes:
     * - Visit top node (and remove from the stack)
     * - Store right child
     * - Store left child
     */
    while(!nodeStack.empty()) {
        Node* currentNode = nodeStack.top();
        nodeStack.pop();

        idStorage[currentIndex++] = currentNode->id;

        if(currentNode->right) {
            nodeStack.push(currentNode->right);
        }

        if(currentNode->left) {
            nodeStack.push(currentNode->left);
        }
    }

    return idStorage;
}


Node* BinaryTree::buildTree(int inOrdr[], int postOrdr[], int n) {
    int indx = n - 1;
    /**
     * Root node is result of buildAux reading all the data from the right
     */
    return buildAux(inOrdr, postOrdr, 0, n - 1, &indx);
}


Node* BinaryTree::buildAux(
    int inOrdr[],
    int postOrdr[],
    int startIndex,
    int endIndex,
    int* postOrderIndex
) {
    /**
     * No more items to read
     */
    if (startIndex > endIndex)
        return NULL;

    /**
     * Pick current node from postOrder traversal using
     * postOrderIndex and decrement postOrderIndex
     */
    Node* node = new Node(postOrdr[*postOrderIndex]);
    (*postOrderIndex)--;

    /**
     * If this node has no children then return
     */
    if (startIndex == endIndex)
        return node;

    /**
     * Else find the index of this node in InOrder traversal
     */
    int inOrderIndex = searchInOrder(inOrdr, startIndex, endIndex, node->id);

    /**
     * Using index in Inorder traversal, construct left and right subtress
     */
    node->right = buildAux(inOrdr, postOrdr, inOrderIndex + 1, endIndex, postOrderIndex);
    node->left = buildAux(inOrdr, postOrdr, startIndex, inOrderIndex - 1, postOrderIndex);

    return node;
}


int BinaryTree::searchInOrder(
    int arr[],
    int startIndex,
    int endIndex,
    int value
) {
    int index;
    for(index = startIndex; index <= endIndex; index++) {
        if(arr[index] == value)
            break;
    }
    return index;
}


void BinaryTree::deleteNode(Node* node) {
    if(node->left) {
        deleteNode(node->left);
    }

    if(node->right) {
        deleteNode(node->right);
    }

    delete node;
}
