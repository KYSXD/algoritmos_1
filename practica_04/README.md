# Practica 4

## Descripción

### Objetivos

- Familiarizar al estudiante con los recorridos en arboles binarios.
- Que el estudiante sea capaz de construir un árbol binario y hacer una búsqueda sobre el mismo.

### Ejercicio

Se te darán dos arreglos del mismo tamaño y su tamaño.
El primero representa el recorrido de un árbol en inorder y el segundo en posorder.
Deberás imprimir el recorrido del árbol en preorder.
Para esto primero deberás construir el árbol y luego hacer el recorrido del árbol en preorder.

En esta practica deberás implementar una clase ArbolBinario.
Con un constructor que reciba los arreglos como parámetro y
debe tener un método RecorridoPreorder que cree de forma dinámica y
devuelva un arreglo con el recorrido en preorder.
Igual tenemos una estructura nodo que nos representa los nodos del árbol.

Puedes añadir las funciones que creas correspondientes para complementar la clase.
Sin embargo, esas funciones deberían ser privadas.

No se les olvide implementar el destructor que deberá borrar la memoria correspondiente a cada nodo.
(Esto implica que cada nodo deberá ser creado con memoria dinámica).

Se te asegura que los arreglos que se te dan siempre representan
un recorrido Inorder y Posorder de un árbol binario.

### Compilación

De preferencia llamar a su libería _"ArbolBinario.hpp"_
y las funciones que estén definidas en _"ArboBinario.cpp"_
para que se pueda ejecutar con el comando:

```bash
$ g++ --std=c++11 main.cpp ArbolBinario.cpp -o ArbolBinario
```

*Nota del desarrollador: Dada la estructura con la que se implementó,
se sugiere usar la siguiente instrucción:*

```bash
$ g++ --std=c++11 main.cpp binaryTree.cpp -o main.o
```

## Reporte

### Marco teórico

Un arból binario es una estructura de datos
en la que cada nodo puede tener a lo más dos hijos,
uno izquierdo y derecho.

Un recorrido de árbol
(también conocido como busqueda o caminata)
se refiere al proceso de visitar sistemáticamente
cada nodo en una estructura de árbol.

Estos recorridos pueden ser clasificados
por el orden en que los nodos son visitados,
siendo relevantes para este caso:

- PreOrder:
  - Se visita al nodo padre
  - Se recorre el hijo izquierdo
  - Se recorre el hijo derecho
- PostOrder:
  - Se recorre el hijo izquierdo
  - Se recorre el hijo derecho
  - Se visita al nodo padre
- InOrder:
  - Se recorre el hijo izquierdo
  - Se visita al nodo padre
  - Se recorre el hijo derecho

### Descripción del programa

La libreria consta de una clase
`BinaryTree` que describe un arból binario
con un constructor que recibe dos arreglos
que representan un recorrido `InOrder` y un recorrido `PostOrder`.

El constructor se apoya de una función
`buildAux`,
que se encarga de construir recursivamente
cada nodo del árbol iterando segun el recorrido
`postOrder` en orden inverso,
auxiliado por una funcion `searchInOrder`
que se encarga de devolver el indice
de cada elemento en el recorrido `InOrder`
para describir los hijos de cada uno.

Además,
el método principal `main()`
crea un arbol e imprime el recorrido `PreOrder`
bajo las reglas definidas.

Se añade una cabecera adicional `ArbolBinario.hpp` y
una redefinción del tipo `BinaryTree` como `ArbolBinario`.
También se incluye un alias a la función `preOrderTraversal`
a traves de la función `inline int* recorrido_preorder()`.
Esto para evitar la modificación del `main.cpp` provisto.

Adicionalmente se añade una libería `stack.hpp`
que implementa una plantilla para clases `Stack` de cualquier tipo,
usada en la implementación de la función `preOrderTraversal` de la clase `BinaryTree`.

### Resultados obtenidos

Se crea y manipula satisfactoriamente un árbol binario tal como se requiere.

### Problemas encontrados

Dado el requerimiento de regresar un arreglo con el recorrido `PreOrder`
y la incertidumbre sobre la posibilidad de usar el `stack` del `stl`,
se implementó la libería `stack.hpp`.

### Referencias

- [Joma Tech | BinaryTree-related Video](https://youtu.be/OTfp2_SwxHk)
- La clase donde se mencionó el tema.
- https://en.cppreference.com
- [Jenny's lectures CS/IT NET&JRF | Video](https://youtu.be/s5XRtcud35E)
- [Wikipedia | Tree traversal](https://en.wikipedia.org/wiki/Tree_traversal)
- https://www.techiedelight.com/preorder-tree-traversal-iterative-recursive/
- [Check if given inorder and preorder traversals are valid for any Binary Tree without building the tree](https://www.geeksforgeeks.org/check-if-given-inorder-and-preorder-traversals-are-valid-for-any-binary-tree-without-building-the-tree/)
- [Check if given Preorder, Inorder and Postorder traversals are of same tree](https://www.geeksforgeeks.org/check-if-given-preorder-inorder-and-postorder-traversals-are-of-same-tree-set-2/)
