#include "inst.hpp"


class InstQueue {
    private:
        Inst* head = NULL;
        Inst* last = NULL;

    public:
        ~InstQueue();

        void push(Inst *i);
        void pop();
        Inst* top();
        bool isEmpty();
};
