#include <string>
#include "variable.hpp"
#include "variable_stack.hpp"


VariableStack::~VariableStack() {
    Variable* tmp = head;
    while(tmp != NULL) {
        Variable* to_delete = tmp;
        tmp = tmp->next;

        delete(to_delete);
    }
}


void VariableStack::push(Variable *v) {
    if (head == NULL){
        head = v;
    } else {
        v->next = head;
        head = v;
    }
}


void VariableStack::pop() {
    if (head == NULL)
        return;

    Variable *tmp = head;
    head = head->next;

    delete(tmp);
}


Variable* VariableStack::top() {
    return head;
}


bool VariableStack::isEmpty() {
    return head == NULL;
}


Variable* VariableStack::search(std::string name) {
    Variable* current = head;
    while(current) {
        if (current->name == name) {
            break;
        }

        current = current->next;
    }

    return current;
}


bool VariableStack::exists_in_top_scope(std::string name) {
    Variable* current = head;
    while(current) {
        if (current->name == name || current->name == "$$") {
            break;
        }

        current = current->next;
    }

    return current != NULL && current->name != "$$";
}


void VariableStack::remove_scope() {
    while(top() && top()->name != "$$") {
        pop();
    }
    pop();
}
