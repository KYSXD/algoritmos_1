#pragma once
#include <string>


class Inst {
    public:
        char command;
        int value;
        std::string name;
        Inst* next;

        Inst(char command, std::string name, int value);
};
