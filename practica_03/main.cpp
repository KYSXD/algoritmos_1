
#include <iostream>
#include <string>
#include "Estructuras.h"
using namespace std;



void executor(InstQueue &q, StackVariable &varStack){
    while (!q.isEmpty()){
        Inst *inst = q.top();
        if (inst->command == 'I'){
            if (!varStack.exists_in_top_scope(inst->name)){
                varStack.push(new Variable(inst->name, inst->value));
            }else{
                cout << "[ERR] at Insert: var " << inst->name << " already exists in this scope" << endl;
            }
        }else if (inst->command=='A'){
            varStack.push(new Variable("$$", inst->value));
        }else if (inst->command=='D'){
            varStack.remove_scope();
        }else if (inst->command=='B'){
            Variable *v = varStack.search(inst->name);
            if (v==NULL){
                cout << "[ERR] in Search: var " << inst->name << " does not exist" << endl;
            }else{
                cout << "var: " << v->name << " value: " <<v->value << endl;
            }
        }else if (inst->command=='M'){
            Variable *v = varStack.search(inst->name);
            if (v!=NULL){
                v->value = inst->value;
            }else{
                cout << "[ERR] in Mod: var " << inst->name << " does not exist" << endl;
            }
        }
        q.pop();
    }
}


int main()
{
    
    InstQueue q;
    StackVariable stack;
    
    q.push(new Inst('A', "$$", 0));
    q.push(new Inst('I', "x", 10));
    q.push(new Inst('I', "y", 15));
    q.push(new Inst('I', "y", 30));
    q.push(new Inst('A', "$$", 0));
    q.push(new Inst('I', "x", 20));
    q.push(new Inst('I', "z", 5));
    q.push(new Inst('B', "x", 0));
    q.push(new Inst('B', "z", 0));
    q.push(new Inst('D', "--", 0));
    q.push(new Inst('B', "x", 0));
    q.push(new Inst('B', "z", 0));
    q.push(new Inst('M', "x", 100));
    q.push(new Inst('M', "z", 100));
    q.push(new Inst('B', "x", 0));
    
    executor(q, stack);

    return 0;
}
