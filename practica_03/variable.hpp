#pragma once
#include <string>


class Variable {
    public:
        std::string name;
        Variable *next;
        int value;

        Variable(std::string name, int value);
};
