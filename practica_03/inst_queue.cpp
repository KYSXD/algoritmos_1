#include "inst_queue.hpp"
#include "inst.hpp"


InstQueue::~InstQueue() {
    Inst* tmp = head;
    while(tmp != NULL) {
        Inst* to_delete = tmp;
        tmp = tmp->next;

        delete(to_delete);
    }
}


void InstQueue::push(Inst *i) {
    if (head == NULL){
        head = i;
        last = i;
    } else {
        last->next = i;
        last = i;
    }
}


void InstQueue::pop() {
    if (head == NULL)
        return;

    Inst* tmp = head;
    head = head->next;

    delete(tmp);
}

Inst* InstQueue::top() {
    return head;
}


bool InstQueue::isEmpty() {
    return head == NULL;
}
