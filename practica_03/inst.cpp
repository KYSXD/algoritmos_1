#include <string>
#include "inst.hpp"


Inst::Inst(char command, std::string name, int value) :
    command(command),
    value(value),
    name(name)
{
    next = NULL;
}
