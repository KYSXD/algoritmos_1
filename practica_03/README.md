# Practica 3

## Descripción

### Objetivos

- Familiarizar al estudiante con las implementación de las estructuras de pila y cola.
- Proveer un caso de aplicación para estas estructuras en un escenario real.

### Ejercicio

- Implemente las operaciones definidas en la sección **Pila de Variables**.
  Notemos que estas operaciones hacen uso de las operaciones básicas de una pila
  (`push`, `pop`, `top`, `isEmpty`)
  así que también debe implementar estas operaciones.
- Implemente la cola de instrucciones.
  Esta cola debe funcionar sin ninún problema
  únicamente con las operaciones básicas de `push`, `pop`, `top`, `isEmpty`.
- Libere la memoria cada vez que realice un pop de la cola de instrucciones
  o de la pila de variables.

### Compilación

```bash
$ g++ --std=c++11 main.cpp <nombre_archivos]>.cpp -o main
```

*Nota del desarrollador: Se recomienda usar la siguiente instrucción

```bash
$ g++ --std=c++11 main.cpp inst.cpp inst_queue.cpp variable.cpp variable_stack.cpp -o main.o && ./main.o
```

## Reporte

### Marco teórico

Una cola es una estructura de datos
caracterizada por ser una secuencia de datos
en la que la operación de inserción se realiza por un extremo
y la de extracción por el otro,
siendo el modo de acceso a sus elementos de tipo FIFO.

Una pila es una estructura de datos
caracterizada por ser una secuencia de datos
en la que la operación de inserción
y la de extracción sen realizan por un mismo extremo.
siendo el modo de acceso a sus elementos de tipo LIFO.

### Descripción del programa

La librería consta de 4 clases,
`Inst`, `InstQueue`, `Variable` y `VariableStack`,
que describen
una instrucción,
una cola de instrucciones,
una variable
y una pila de variables, respectivamente.

Además,
el método principal `main()`
crea una pila de variables,
crea una cola de instrucciones
a la cual añade instrucciones
y delega al método `executor()`
la ejecución de las mismas,
bajo las reglas definidas.

Se añade una cabecera adicional `Estructuras.h` y
una redefinción del tipo `VariableStack` como `StackVariable`
para evitar la modificación del `main.cpp` provisto.

### Resultados obtenidos

Se crean y manipulan satisfactoriamente una pila y una cola tal como se requiere.

### Problemas encontrados

El ejemplo provisto en la sección `Entradas`
no coincide con la salida obtenida.
Se sugiere la idea de que las entradas
para el ejemplo y las indicadas en el archivo `main.cpp`
son distintas.

### Referencias

- La clase donde se mencionó el tema.
- https://en.cppreference.com
- https://www.geeksforgeeks.org/queue-data-structure/
- https://www.geeksforgeeks.org/stack-data-structure/
