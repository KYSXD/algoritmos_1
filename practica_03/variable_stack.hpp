#pragma once
#include <string>
#include "variable.hpp"


class VariableStack {
    private:
        Variable* head = NULL;

    public:
        ~VariableStack();

        // basic operations
        void push(Variable *v);
        void pop();
        Variable* top();
        bool isEmpty();

        // extra operations
        Variable* search(std::string name);
        bool exists_in_top_scope(std::string name);
        void remove_scope();
};
