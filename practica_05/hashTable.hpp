#include <iostream>
#include <string>
#include <list>
using namespace std;


class HashTable {
    public:
        // constructor
        HashTable(unsigned int n);
        // destructor
        ~HashTable();

        // insert or update
        void insert(string key, int value);
        // find key-value pair
        pair<string, int>* find(string key);

    private:
        // size of hash table
        unsigned int N;
        // hash function
        unsigned int hash(string key);
        //  key-value array
        list<pair<string, int>>* key_values;
};
