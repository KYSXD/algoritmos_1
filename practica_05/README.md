# Practica 5

## Descripción

### Objetivos

- Familiarizar al estudiante con la estructura Tabla Hash y su implementación.
- Proporcionar al estudiante con un ejemplo de utilidad de una Tabla Hash.

### Ejercicio

1. Implemente el constructor de la tabla hash que reciba como parámetro
  el tamaño inicial del areglo que almacenará las listas,
  que a su vez almacenan los pares de __key-value__.
2. Defina su propia función de has para indexar a una posición
  de su arreglo de listas enlazadas.
  La función hash debe procesar cadenas como lo podemos ver
  en la firma de la clase.
  Puede auxiliarse de funciones de la _stl_ como
  __std::hash<std::string>{}(str)__
  que se encargan de generar un hash para una cadena.
3. Implemente la función de buscar en la tala hash.
  Notemos que esta función regresa un puntero del objeto pair.
  Esto es por el caso en el cual queramos actualizar el valor
  del par key-value,
  ya tenemos la referencia y podemos actualizar su valor sin tener que
  insertar nuevamente el valor en la tabla hash.
4. Implemente la función de insert de la tabla hash.
  Debido a que solucionamos las colisiones con una lista enlazada,
  esta función debe buscar inicialmente si la llave ya existe
  en la lista enlazada.
  En caso de que no exista, insertamos la nueva llave con su valor.
  Al momento de insertar en la lista enlazada utilice
  la función __list.push_back__ de la STL.
  Esta funcín inserta el nodo deseado al final de la lista,
  lo cual se puede hacer en tiempo constante teniendo un puntero
  al final de la lista.
  Notemos entonces que la inserción siempre se realiza en tiempo constante.
  La búsqueda para verificar si el valor existe es la que es
  en el peor caso de orden lineal,
  pero con una buena función hash
  podemos llegar a un orden amortiguado constante,
  es decir se espera que en promedio el tiempo de búsqueda
  sea constante debido a que la mayoría de las listas enlazadas
  deberían contener pocos nodos o incluso solo un nodo.
  __NOTA:__ Para esta función debe realizar la búsqueda de la lalve
  para ver si existe,
  no debe escribir nuevamente el código de búsqueda en la inserción,
  utilice su función para buscar que ya implementó anteriormente.
5. Defina una función update
  __(de preferencia en el archivo main.cpp,
  la función ya se encuentra declarada en el archivo main.cpp adjunto,
  solo debe escribir el código)__
  que resuelva el problema del vocabulario.
  Es decir, se debe insertar las palabaras en la tabla hash.
  Si la palabra a insertar no existe,
  inserta la palabra con valor de 1,
  es decir la palabara tiene una cuenta de 1 en el vocabulario.
  Si la palabra existe,
  entonces debe incrementar una unidad su valor,
  debido a que significa que se encontró otra coincidencia para la palabra
  y por lo tanto su cuenta debe aumentar en 1.
6. Libere la memoria reservada para la tabla hash con el arreglo dinámico.

### Compilación

```bash
$ g++ --std=c++11 main.cpp HashTable.cpp -o main
```

*Nota del desarrollador: Dada la estructura con la que se implementó,
se sugiere usar la siguiente instrucción:*

```bash
$ g++ --std=c++11 main.cpp hashTable.cpp -o main.o
```

## Reporte

### Marco teórico

Una tabla hash es una estructura de datos asociativa
que asocia llaves con valores.
Se caracteriza por soportar de manera eficiente las busquedas por llaves,
usando una función __hash__.

En la practica, una __funcion hash__
funciona como una proyección de los datos sobre el conjunto de llaves disponibles.

Una implementación útil de la tabla hash se auxilia de listas
doblemente ligadas para manejar las colisiones y amortiguar la inserción y borrado de entradas.

### Descripción del programa

La libreria consta de una clase `HashTable`
con los metodos:
- `insert` que guarda el (llave, coincidencias) en
  la entrada del arreglo `key_value` o actualiza su valor si este existe.
- `find` que devuelve la dirección del par (llave, coincidencias) correspondiente si
  este existe en la tabla, o `NULL` en caso contrario.
- `hash` que calcula el valor hash de una cadena usando la __stl__.

Ademas, el archivo `main.cpp` implementa dos funciones
- `query_word` que busca una cadena en la tabla.
- `update` que busca una cadena en la tabla y la añade si no existe en la tabla
  o la actualiza incrementando su contador de coincidencias en 1.

### Resultados obtenidos

Se crea y manipula satisfactoriamente una tabla hash tal como se requiere.

### Problemas encontrados

Ninguno.

### Referencias

- La clase donde se mencionó el tema.
- La ayudantia donde se mencionó el tema.
- https://en.cppreference.com
- [Joma Tech | HashTable-semi-related Video](https://youtu.be/pKO9UjSeLew)
