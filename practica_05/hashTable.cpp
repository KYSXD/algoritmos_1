#include <functional>
#include <list>
#include <tuple>
#include <string>
#include "hashTable.hpp"


/**
 * Sets N and allocates an array of length n in key_values
 */
HashTable::HashTable(unsigned int n) : N(n) {
    key_values = new std::list<std::pair<std::string, int>>[n];
}


/**
 * Deallocates key_values
 */
HashTable::~HashTable() {
    delete[] key_values;
}


/**
 * Insert or update the pair (key,value)
 *
 * Searches for match with key
 * If match exists, updates value
 * If don't, appends to corresponding entry by hash_value
 */
void HashTable::insert(std::string key, int value) {
    std::pair<std::string, int>* match = find(key);

    if(match) {
        match->second = value;
    } else {
        unsigned int hash_value = hash(key);
        key_values[hash_value].push_back(
            std::make_pair(key, value)
        );
    }
}


/**
 * Searchs the pair (key,value)
 *
 * Finds entry in key_values and iterates over the list
 */
std::pair<std::string, int>* HashTable::find(std::string key) {
    unsigned int item_hash = hash(key);

    std::list<std::pair<std::string, int>>* current_list = &key_values[item_hash];
    for(
        std::list<std::pair<std::string, int>>::iterator it = current_list->begin();
        it != current_list->end();
        ++it
    ) {
        if(it->first == key) {
            return &(*it);
        }
    }

    return NULL;
}


/**
 * Calculates hash_value
 *
 */
unsigned int HashTable::hash(std::string key) {
    return std::hash<std::string>{}(key) % N;
}
