# Practica 8

## Descripción

### Objetivos

- Familiarizar al estudiante con los algoritmos de bubble sort y selection sort.
- Familiarizar al estudiante con el uso de plantillas.

### Enunciado

Implemente el algoritmo de bubble sort y selection sort con plantillas
para ordenar de manera ascendente un arreglo de valores.

No es necesario crear más archivos,
__programe ambos métodos en el archivo main.cpp__

#### Compilación

```bash
$ g++ --std=c++11 main.cpp -o main
```

*Nota del desarrollador: Se sugiere usar la siguiente instrucción:*

```bash
$ g++ --std=c++11 main.cpp -o main.o
```

## Reporte

### Marco teórico

Bubble sort u ordenamiento de burbuja es un algoritmo de ordenamiento
que funciona
revisando cada elemento de la lista que va a ser ordenada
con el siguiente,
intercambiándolos de posición si están en el orden equivocado.

Selection sort u ordenamiento por selección es un algoritmo de ordenamiento
que funciona
buscando el mínimo elemento entre una posición y el final de la lista,
intercambiándolos de posición,
incrementando la posición inicial hasta recorrer toda la lista.

### Descripción del programa

Se implementan dos funciones,
bubbleSort y selectionSort, con plantillas,
para ordenar de manera ascendente un arreglo de valores.

### Resultados obtenidos

Se ordenan arreglos de manera ascendente
con los algoritmos de ordenamiento solicitados
tal como se requiere.

### Problemas encontrados

Ninguno.

### Referencias

- La clase donde se mencionó el tema.
