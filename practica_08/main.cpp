#include <iostream>
#include <string>
#include <algorithm>

using namespace std;

// ******************************************************************* Custom Class to test Sorting *******************************************************************

class Persona{
  public:
  string nombre;
  unsigned int edad;

  Persona(){}

  bool operator <(Persona p){
      return this->edad < p.edad;
  }

  bool operator >(Persona p){
      return this->edad > p.edad;
  }

  bool operator ==(Persona p){
      return this->edad == p.edad;
  }

  bool operator !=(Persona p){
      return this->edad != p.edad;
  }
};


// ******************************************************************* print functions *******************************************************************


template <typename tipo>
void print(tipo data[], unsigned int n){
    for (unsigned int i = 0; i<n; i++){
        cout << data[i] << endl;
    }
}

void printPersona(Persona p_arr[], unsigned int n){
    for (unsigned int i = 0; i<n; i++){
        cout << p_arr[i].nombre << " edad: " << p_arr[i].edad << endl;
    }
}


// ******************************************************************* SORTING ALGORITHMS HERE *******************************************************************

template <typename T>
void bubbleSort(T data[], unsigned int n) {
    bool swapped = true;
    while(swapped) {
        swapped = false;
        for(unsigned int i = 0; i < n-1; i++) {
            if(data[i] > data[i+1]) {
                std::swap(data[i], data[i+1]);
                swapped = true;
            }
        }
    }
}


template <typename T>
void selectionSort(T data[], unsigned int n) {
    T min;
    int temp = 0;

    for(int step = 0; step < n-1; step++) {
        min = data[step];
        temp = step;
        for(int i = step + 1; i < n; i++) {
            if(min > data[i]) {
                min = data[i];
                temp = i;
            }
        }

        std::swap(data[step], data[temp]);
    }
}


// *********************************************************************** Driver Progrma *************************************************************************

int main()
{
    //int arr[10] = {-1, -10, 20, 30, 50, 5, -5, 7, 9, 15};
    //int insArr[10] = {-1, -10, 20, 30, 50, 5, -5, 7, 9, 15};
    string arr[10] = {"andrea", "juan", "hola", "mundo", "cabrera", "pedro", "camion", "ocho", "nuevo", "aleluya"};
    string insArr[10] = {"andrea", "juan", "hola", "mundo", "cabrera", "pedro", "camion", "ocho", "nuevo", "aleluya"};

    // bubble sort
    cout << "Bubble Sort" <<endl;
    bubbleSort(arr, 10);
    print(arr, 10);

    // selection sort
    cout << "Selection Sort" <<endl;
    selectionSort(insArr, 10);
    print(insArr, 10);
    cout << endl;

    // sorting people by age
    string nombres[10] = {"andrea", "juan", "paco", "jesus", "cabrera", "pedro", "Luisa", "Luis", "Jose", "Francisco"};
    unsigned edades[10] = {18, 23, 15, 15, 10, 25, 35, 7, 24, 29};
    Persona p_arr[10];

    for (unsigned int i = 0; i<10; i++){
        p_arr[i].nombre = nombres[i];
        p_arr[i].edad = edades[i];
    }

    // bubble sort with Persona object
    cout << "Sorting People" <<endl;
    bubbleSort(p_arr, 10);
    printPersona(p_arr, 10);

    return 0;
}
