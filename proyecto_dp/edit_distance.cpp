#include <algorithm>
#include <iostream>
#include <vector>


inline int min_3(int x, int y, int z)
{
  return std::min(std::min(x, y), z);
}


int edit_distance_util(
  const std::string& str1,
  const std::string& str2,
  int l1,
  int l2,
  std::vector< std::vector<int> >& memory_table
) {
  if (l1 == 0) {
    return l2;
  }

  if (l2 == 0) {
    return l1;
  }

  if (memory_table[l1 - 1][l2 - 1] != -1) {
    return memory_table[l1 - 1][l2 - 1];
  }

  if (str1[l1 - 1] == str2[l2 - 1]) {
    memory_table[l1 - 1][l2 - 1] = edit_distance_util(str1, str2, l1 - 1, l2 - 1, memory_table);

    return memory_table[l1 - 1][l2 - 1];
  }

  int insert_case = edit_distance_util(str1, str2, l1, l2 - 1, memory_table);
  int remove_case = edit_distance_util(str1, str2, l1 - 1, l2, memory_table);
  int replace_case = edit_distance_util(str1, str2, l1 - 1, l2 - 1, memory_table);

  memory_table[l1 - 1][l2 - 1] = 1 + min_3(
    insert_case,
    remove_case,
    replace_case
  );

  return memory_table[l1 - 1][l2 - 1];
}


int edit_distance(
  const std::string& s,
  const std::string& v
) {
  int l1 = s.length();
  int l2 = v.length();

  std::vector< std::vector<int> > memory_table(
    l1,
    std::vector<int>(l2, -1)
  );

  return edit_distance_util(s, v, l1, l2, memory_table);
};


inline int dist_edicion(
  const std::string& s,
  const std::string& v
) {
  return edit_distance(s, v);
};


int main() {
    std::cout << "La distancia entre sentar y centrar es "
    << dist_edicion("sentar", "centrar") << '\n'
    << "La distancia entre pasear y marear es "
    << dist_edicion("pasear", "marear") << '\n'
    << "La distancia entre rodear y circular es "
    << dist_edicion("rodear", "circular") << '\n';
    return 0;
}
