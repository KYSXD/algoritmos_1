#include <iostream>
#include <limits>
#include <vector>


long long min_matrix_chain_ops_util(
  const std::vector<std::pair<int, int>>& dimensions,
  int i,
  int j,
  std::vector< std::vector<long long> >& memory_table
) {
  if (i == j) {
    return 0;
  }

  if (memory_table[i][j] != -1) {
    return memory_table[i][j];
  }

  memory_table[i][j] = std::numeric_limits<long long>::max();
  for (int k = i; k < j; k++) {
    long long left = min_matrix_chain_ops_util(dimensions, i, k, memory_table);

    long long right = min_matrix_chain_ops_util(dimensions, k + 1, j, memory_table);

    long long current = dimensions[i].first * dimensions[k].second * dimensions[j].second;

    memory_table[i][j] = std::min(
      memory_table[i][j],
      left + right + current
    );
  }

  return memory_table[i][j];
}


long long min_matrix_chain_ops(
  const std::vector<std::pair<int, int>>& dimensions
) {
  int l = dimensions.size();

  std::vector< std::vector<long long> > memory_table(
    l + 1,
    std::vector<long long>(l + 1, -1)
  );

  return min_matrix_chain_ops_util(dimensions, 0, l - 1, memory_table);
}


inline long long int mejor_multiplicacion (
  const std::vector<std::pair<int, int>>& Dimensiones
) {
  return min_matrix_chain_ops(Dimensiones);
}


int main() {
    std::pair<int, int> A = {10, 30},
                        B = {30, 5},
                        C = {5, 60},
                        D = {5, 35},
                        E = {35, 25},
                        F = {60, 35};

    std::cout << "Multiplicar A x B x C requiere al menos "
              << mejor_multiplicacion({A, B, C}) << " operaciones\n"
              << "Multiplicar A x B x D x E requiere al menos "
              << mejor_multiplicacion({A, B, D, E}) << " operaciones\n"
              << "Multiplicar A x B x C x F x E requiere al menos "
              << mejor_multiplicacion({A, B, C, F, E}) << '\n';
    return 0;
}
