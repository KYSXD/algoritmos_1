#include <algorithm>
#include <iostream>
#include <vector>


inline int min_3(int x, int y, int z)
{
  return std::min(std::min(x, y), z);
}


int edit_distance_util(
  const std::string& str1,
  const std::string& str2,
  int l1,
  int l2,
  std::vector< std::vector<int> >& memory_table
) {
  static int scope = 0;
  std::cout << std::string(scope++ * 4, ' ')
    << "Check \""
    << str1.substr(0, l1)
    << "\" vs \""
    << str2.substr(0, l2)
    << "\"" << std::endl;

  if (l1 == 0) {
    std::cout << std::string(scope-- * 4, ' ')
      << "Return " << l2 << std::endl;
    return l2;
  }

  if (l2 == 0) {
    std::cout << std::string(scope-- * 4, ' ')
      << "Return " << l1 << std::endl;
    return l1;
  }

  if (memory_table[l1 - 1][l2 - 1] != -1) {
    std::cout << std::string(scope-- * 4, ' ')
      << "Return " << memory_table[l1 - 1][l2 - 1]
      << " (From previously stored)" << std::endl;
    return memory_table[l1 - 1][l2 - 1];
  }

  if (str1[l1 - 1] == str2[l2 - 1]) {
    std::cout << std::string(scope * 4, ' ')
      << "(Same last character, checking subcase)" << std::endl;
    memory_table[l1 - 1][l2 - 1] = edit_distance_util(str1, str2, l1 - 1, l2 - 1, memory_table);

    std::cout << std::string(scope-- * 4, ' ')
      << "Return " << memory_table[l1 - 1][l2 - 1]
      << " (Same last character, subcase returned)" << std::endl;
    return memory_table[l1 - 1][l2 - 1];
  }

  std::cout << std::string(scope * 4, ' ') << "(Insert case)" << std::endl;
  int insert_case = edit_distance_util(str1, str2, l1, l2 - 1, memory_table);
  std::cout << std::string(scope * 4, ' ') << "(Remove case)" << std::endl;
  int remove_case = edit_distance_util(str1, str2, l1 - 1, l2, memory_table);
  std::cout << std::string(scope * 4, ' ') << "(Replace case)" << std::endl;
  int replace_case = edit_distance_util(str1, str2, l1 - 1, l2 - 1, memory_table);

  memory_table[l1 - 1][l2 - 1] = 1 + min_3(
    insert_case,
    remove_case,
    replace_case
  );

  std::cout << std::string(scope-- * 4, ' ')
    << "Return " << memory_table[l1 - 1][l2 - 1]
    << " (Min of "
    << insert_case << ", "
    << remove_case << ", "
    << replace_case
    << ", plus 1)" << std::endl;
  return memory_table[l1 - 1][l2 - 1];
}


int edit_distance(
  const std::string& s,
  const std::string& v
) {
  int l1 = s.length();
  int l2 = v.length();

  std::vector< std::vector<int> > memory_table(
    l1,
    std::vector<int>(l2, -1)
  );

  return edit_distance_util(s, v, l1, l2, memory_table);
};


inline int dist_edicion(
  const std::string& s,
  const std::string& v
) {
  return edit_distance(s, v);
};


int main()
{
  std::cout << dist_edicion("sentar", "centrar") << std::endl;
  std::cout << dist_edicion("pasear", "marear") << std::endl;
  std::cout << dist_edicion("rodear", "circular") << std::endl;

  return 0;
}
