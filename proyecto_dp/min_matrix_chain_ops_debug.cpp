#include <iostream>
#include <limits>
#include <vector>


std::string chain_str(
  const std::vector<std::pair<int, int>>& dimensions,
  int i,
  int j
) {
  std::string res = "";
  for (int k = i; k <= j; k++) {
    res.append("(");
    res.append(std::to_string(dimensions[k].first));
    res.append(", ");
    res.append(std::to_string(dimensions[k].second));
    res.append(")");
    if (k != j) {
      res.append(" ");
    }
  }

  return res;
}


long long min_matrix_chain_ops_util(
  const std::vector<std::pair<int, int>>& dimensions,
  int i,
  int j,
  std::vector< std::vector<long long> >& memory_table
) {
  static int scope = 0;
  std::cout << std::string(scope++ * 4, ' ')
    << "Check chain: " << chain_str(dimensions, i, j) << std::endl;

  if (i == j) {
    std::cout << std::string(scope-- * 4, ' ')
      << "Return 0 (self case)" << std::endl;
    return 0;
  }

  if (memory_table[i][j] != -1) {
    std::cout << std::string(scope-- * 4, ' ')
      << "Return " << memory_table[i][j] << " (From previously stored)" << std::endl;
    return memory_table[i][j];
  }

  memory_table[i][j] = std::numeric_limits<long long>::max();
  for (int k = i; k < j; k++) {
    std::cout << std::string(scope++ * 4, ' ')
      << "(Calculate with chains " << i << "-" << k << " and " << k + 1 << "-" << j << ")" << std::endl;

    std::cout << std::string(scope++ * 4, ' ') << "(Calculate left chain)" << std::endl;
    long long left = min_matrix_chain_ops_util(dimensions, i, k, memory_table);
    scope--;

    std::cout << std::string(scope++ * 4, ' ') << "(Calculate right chain)" << std::endl;
    long long right = min_matrix_chain_ops_util(dimensions, k + 1, j, memory_table);
    scope--;

    std::cout << std::string(scope * 4, ' ') << "(Check integration)" << std::endl;
    long long current = dimensions[i].first * dimensions[k].second * dimensions[j].second;
    std::cout << std::string((scope + 1) * 4, ' ')
      << "Compare sum of "
      << left << " (left) "
      << right << " (right) "
      << current << " ("
      << dimensions[i].first << "*"
      << dimensions[k].second << "*"
      << dimensions[j].second <<") "
      << "against min"
      << std::endl;

    memory_table[i][j] = std::min(
      memory_table[i][j],
      left + right + current
    );

    std::cout << std::string(scope-- * 4, ' ')
      << "Current min: " << memory_table[i][j] << std::endl;
  }

  std::cout << std::string(scope-- * 4, ' ')
    << "Return " << memory_table[i][j] << " (min)" << std::endl;
  return memory_table[i][j];
}


long long min_matrix_chain_ops(
  const std::vector<std::pair<int, int>>& dimensions
) {
  int l = dimensions.size();

  std::vector< std::vector<long long> > memory_table(
    l + 1,
    std::vector<long long>(l + 1, -1)
  );

  return min_matrix_chain_ops_util(dimensions, 0, l - 1, memory_table);
}


inline long long int mejor_multiplicacion (
  const std::vector<std::pair<int, int>>& Dimensiones
) {
  return min_matrix_chain_ops(Dimensiones);
}


int main() {
    std::pair<int, int> A = {10, 30},
                        B = {30, 5},
                        C = {5, 60},
                        D = {5, 35},
                        E = {35, 25},
                        F = {60, 35};

    std::cout << mejor_multiplicacion({A, B, C}) << std::endl;
    std::cout << mejor_multiplicacion({A, B, D, E}) << std::endl;
    std::cout << mejor_multiplicacion({A, B, C, F, E}) << std::endl;
    return 0;
}
