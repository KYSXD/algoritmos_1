#include <iostream>

static const int N = 10;

int main() {
    int i, p, q, id[N];
    for(i = 0; i < N; i++) id[i] = i;

    for(i = 0; i < N; i++) {
        std::cout << id[i] << " ";
    }
    std::cout << std::endl;

    while(std::cin >> p >> q) {
        int t = id[p];
        if(t == id[q]) continue;

        for(i = 0; i < N; i++) {
            if(id[i] == t) id[i] = id[q];
        }

        for(i = 0; i < N; i++) {
          std::cout << id[i] << " ";
        }
        std::cout << std::endl;

        std::cout << " Pair " << p << ", " << q << " gives a new connection " << std::endl;
    }

    return 0;
}
